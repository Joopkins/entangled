IOTA Entangled Monorepo
=======================================

Building
=============
* `git submodule update --init --recursive`
* Please use https://www.bazel.build/
* `bazel test //...`
